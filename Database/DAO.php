<?php
require_once 'db.php';

class DAO {
	 private $db;
	 public function __construct()
	 {
		 $this->db = DB::createInstance();
	 }
     private $INSERTPRODUCT = "INSERT INTO products (model, type, price, size, color, brand_id) VALUES (?,?,?,?,?,?) ";
     private $SELECTPRODUCT = "CALL selectProducts";
     private $DELETEPRODUCTBYPRODUCTID = "DELETE  FROM PRODUCTS WHERE PRODUCT_id = ?";
     private $SELECTPRODUCTBYCOLOR = "SELECT * FROM products where color = ? ";
	
	 

    public function insertProduct($model, $type, $price, $size, $color, $brand_id)
	{
		$statement = $this->db->prepare($this->INSERTPRODUCT);
		$statement->bindValue(1, $model);
		$statement->bindValue(2, $type);
		$statement->bindValue(3, $price);
		$statement->bindValue(4, $size);
		$statement->bindValue(5, $color);
		$statement->bindValue(6, $brand_id);
		$statement->execute();
	}
    public function selectProduct()
	{
		$statement = $this->db->prepare($this->SELECTPRODUCT);
        $statement ->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}
	public function deleteProductByProductId($id)
	{
		$statement = $this->db->prepare($this->DELETEPRODUCTBYPRODUCTID);
		$statement->bindValue(1,$id);
		$statement->execute();
	}
	public function selectProductByColor($color)
	{
		$statement = $this->db->prepare($this->SELECTPRODUCTBYCOLOR);
		$statement->bindValue(1, $color, PDO::PARAM_STR);
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}
    }
	?>
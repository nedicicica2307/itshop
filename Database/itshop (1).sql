-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 07, 2022 at 06:03 PM
-- Server version: 5.7.34
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `itshop`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `selectProducts` (IN `a` INT)  SELECT * FROM products$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectUserByUsername` (IN `u` VARCHAR(255))  SELECT * FROM users where username = u$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectUserByUsernameAndPasword` (IN `u` VARCHAR(255), IN `p` VARCHAR(255))  SELECT * FROM users join user_atributes on users.user_atributes_id = user_atributes.user_atributes_id where users.username = u and users.password = p$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `manufacturer_id`, `brand_name`) VALUES
(3, 5, 'Samsung'),
(4, 8, 'Sony'),
(5, 6, 'Iphone'),
(6, 6, 'Macbook'),
(7, 7, 'HP'),
(8, 4, 'Huawei'),
(9, 4, 'Honor'),
(10, 3, 'Motorola'),
(11, 3, 'Lenovo');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE `manufacturers` (
  `id` int(11) NOT NULL,
  `manufacturer_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`id`, `manufacturer_name`) VALUES
(3, 'Lenovo\r\n'),
(4, 'Huawei'),
(5, 'Samsung'),
(6, 'Apple'),
(7, 'HP'),
(8, 'Sony');

-- --------------------------------------------------------

--
-- Table structure for table `Ordered_items`
--

CREATE TABLE `Ordered_items` (
  `order_item_id` int(11) NOT NULL,
  `order_item` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `item_details` varchar(255) NOT NULL,
  `orders_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Orders`
--

CREATE TABLE `Orders` (
  `id` int(11) NOT NULL,
  `order_item` varchar(255) NOT NULL,
  `delevery_address` varchar(255) NOT NULL,
  `shopping_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `model` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `size` int(11) NOT NULL,
  `color` varchar(255) NOT NULL,
  `brand_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `model`, `type`, `price`, `size`, `color`, `brand_id`) VALUES
(1, 'Galaxy a72', 'phone', 250, 6, 'black', 3),
(2, 'Sony Xperia', 'phone', 150, 6, 'blue', 4),
(3, 'Huawei p40 Lite', 'phone', 300, 8, 'darkblue', 8),
(4, 'Honor 9 Lite', 'phone', 250, 9, 'black', 9),
(5, 'HP Pavilion', 'laptop', 600, 13, 'white', 7),
(6, 'Iphone 12 Pro', 'phone', 1000, 8, 'mint', 5),
(7, 'Macbook Pro 14', 'laptop', 1500, 14, 'blue', 6),
(8, 'Lenovo Yoga 9', 'laptop', 700, 14, 'black', 11),
(9, 'Motorola g9 plus', 'phone', 300, 9, 'blue', 10),
(10, 'Galaxy a72', 'phone', 250, 6, 'black', 3),
(11, 'Galaxy a72', 'phone', 250, 6, 'black', 3),
(12, 'Galaxy a72', 'phone', 250, 6, 'black', 3),
(13, 'Galaxy a72', 'phone', 250, 6, 'black', 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_atributes_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `id_type`, `type`, `username`, `password`, `user_atributes_id`) VALUES
(5, 1, 'Admin', 'admin1', 'admin1', 1),
(6, 2, 'User', 'user1', 'user1', 2),
(7, 2, 'User', 'user2', 'user2', 3),
(8, 3, 'Moderator', 'moderator1', 'moderator1', 4);

-- --------------------------------------------------------

--
-- Table structure for table `users_types`
--

CREATE TABLE `users_types` (
  `id_type` int(11) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_types`
--

INSERT INTO `users_types` (`id_type`, `type`) VALUES
(1, 'admin'),
(2, 'user'),
(3, 'moderator');

-- --------------------------------------------------------

--
-- Table structure for table `user_atributes`
--

CREATE TABLE `user_atributes` (
  `user_atributes_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_atributes`
--

INSERT INTO `user_atributes` (`user_atributes_id`, `first_name`, `last_name`, `email`) VALUES
(1, 'Marko', 'Jovanovic', 'mar@mar.com'),
(2, 'Jovana', 'Jovanovic', 'jov@jov.com'),
(3, 'Dusko', 'Minic', 'dus@min.com'),
(4, 'Milica', 'Mirkovic', 'mil@mir.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`),
  ADD KEY `manufacturer_id` (`manufacturer_id`);

--
-- Indexes for table `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Ordered_items`
--
ALTER TABLE `Ordered_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `orders_item_id` (`orders_id`);

--
-- Indexes for table `Orders`
--
ALTER TABLE `Orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `brand_id` (`brand_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `id_user_atributes_2` (`user_atributes_id`),
  ADD KEY `id_user_atributes` (`user_atributes_id`),
  ADD KEY `id_type` (`id_type`);

--
-- Indexes for table `users_types`
--
ALTER TABLE `users_types`
  ADD PRIMARY KEY (`id_type`);

--
-- Indexes for table `user_atributes`
--
ALTER TABLE `user_atributes`
  ADD PRIMARY KEY (`user_atributes_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Ordered_items`
--
ALTER TABLE `Ordered_items`
  MODIFY `order_item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Orders`
--
ALTER TABLE `Orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users_types`
--
ALTER TABLE `users_types`
  MODIFY `id_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_atributes`
--
ALTER TABLE `user_atributes`
  MODIFY `user_atributes_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `brands`
--
ALTER TABLE `brands`
  ADD CONSTRAINT `brands_ibfk_1` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Ordered_items`
--
ALTER TABLE `Ordered_items`
  ADD CONSTRAINT `ordered_items_ibfk_1` FOREIGN KEY (`orders_id`) REFERENCES `Orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Orders`
--
ALTER TABLE `Orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`brand_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `users_types` (`id_type`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`user_atributes_id`) REFERENCES `user_atributes` (`user_atributes_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

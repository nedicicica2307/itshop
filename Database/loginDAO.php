<?php
require_once 'db.php';

class loginDAO {
	private $db;

	
	private $SELECT_USER_BY_USERNAME_AND_PASSWORD = "CALL selectUserByUsernameAndPassword(?,?)";	
	private $SELECT_USER_BY_USERNAME = "CALL selectUserByUsername(?)";	
	private $INSERT_USER_ATRIBUTES = "CALL insertUserAtributes (?,?,?) ";	
	private $INSERT_USER = "INSERT into users (type, username, password, id_user_atribute) values ('kupac',?,?,?) ";	
	private $SELECT_PRODUCTS = "CALL selectProducts()";	
	
	public function __construct()
	{
		$this->db = DB::createInstance();
	
	}


	public function selectUserByUsernameAndPassword($username, $password)
	{
		
		$statement = $this->db->prepare($this->SELECT_USER_BY_USERNAME_AND_PASSWORD);
		$statement->bindValue(1, $username);
		$statement->bindValue(2, $password);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}

	public function selectUserByUsername($username)
	{
		
		$statement = $this->db->prepare($this->SELECT_USER_BY_USERNAME);
		$statement->bindValue(1, $username);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}

	private function insertUserAtributes($name, $lastname, $email)
	{
		
		$statement = $this->db->prepare($this->INSERT_USER_ATRIBUTES);
		$statement->bindValue(1, $name);
		$statement->bindValue(2, $lastname);
		$statement->bindValue(3, $email);
		
		$statement->execute();
		
		return $this->db->lastInsertID();
	}
	private function insertUser($username, $password, $id_user_atributes)
	{
		
		$statement = $this->db->prepare($this->INSERT_USER);
		$statement->bindValue(1, $username);
		$statement->bindValue(2, $password);
		$statement->bindValue(3, $id_user_atributes);
		
		$statement->execute();
	}
	public function insertUserWithAtributes($name, $lastname, $email,$username, $password)
	{
		try{
			$this->db->beginTransaction(); 
			$id_user_atributes  = $this->insertUserAtributes($name, $lastname, $email);
			$this->insertUser($username, $password, $id_user_atributes);
			$this->db->commit();			
			return true;
		}catch(PDOException $e){
			$this->db->rollback();			
			return false;
		}
	}

	public function selectProducts()
	{
		
		$statement = $this->db->prepare($this->SELECT_PRODUCTS);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}

}
?>

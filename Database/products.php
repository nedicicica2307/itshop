<?php
    require_once 'DAO.php';
    $dao = new DAO();
   $products = $dao->selectProduct();
  
    if (!isset($_POST['submit'])) {
if (isset($_POST['products'])){
    foreach($_POST['products'] as $product){
        echo $product;
    }
}
}

?>

<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="products.css">

   
</head>
<body>
    <table>
      <tr>
        <th>product_id</th>
        <th>model</th>
        <th>type</th>
        <th>price</th>
        <th>size</th>
        <th>color</th>
        <th>brand_id</th>
      </tr>

<?php 
    foreach ($products as $product ){
    //var_dump($product);
?>
      
      <tr>
        <td><?= $product['product_id'] ?></td>
        <td><?= $product['model'] ?></td>
        <td><?= $product['type'] ?></td>
        <td><?= $product['price'] ?></td>
        <td><?= $product['size'] ?></td>
        <td><?= $product['color'] ?></td>
        <td><?= $product['brand_id'] ?></td>
        <?php } ?>
      </tr>
    </table>
    <br><br><br>

    <form action="products.php" method= "post">
    <div class= "checkboxes">
    <div>
        <h3>Model</h3>
        <input name="products[]" type="checkbox"  id="galaxy" >
        <label for="galaxy">Galaxy a72</label> <br>
        <input name="products[]" type="checkbox"  id="sony-xperia">
        <label for="sony-xperia">Sony Xperia</label> <br>
        <input name="products[]" type="checkbox"  id="Huaweip40Lite" >
        <label for="Huaweip40Lite">Huawei p40 Lite</label><br>
        <input name="products[]" type="checkbox"  id="Honor9Lite" >
        <label for="Honor9Lite">Honor9 Lite</label><br>
        <input name="products[]" type="checkbox"  id="HPPavilion" >
        <label for="HPPavilion">HP Pavilion</label><br>
        <input name="products[]" type="checkbox"  id="iphone">
        <label for="iphone">Iphone</label><br>
        <input name="products[]" type="checkbox"  id="macbook">
        <label for="macbook">Macbook</label><br>
        <input name="products[]" type="checkbox"  id="LenovoYoga9">
        <label for="LenovoYoga9">Lenovo Yoga 9</label><br>
        <input name="products[]" type="checkbox"  id="motorola" >
        <label for="motorola">Motorola</label><br>
    </div>

    <div>
        <h3>Type</h3>
        <input name="products[]" type="checkbox" id="phone">
        <label for="phone">phone</label> <br>
        <input name="products[]" type="checkbox" id="laptop">
        <label for="laptop">laptop</label> <br>
    </div>
               
    <div>
        <h3>Size</h3>
        <input  name="products[]" type="checkbox" id="6px">
        <label for="6px">6px</label> <br>
        <input  name="products[]" type="checkbox" id="8px">
        <label for="8px">8px</label> <br>
        <input  name="products[]" type="checkbox" id="9px">
        <label for="9px">9px</label> <br>
        <input  name="products[]" type="checkbox" id="13px">
        <label for="13px">13px</label><br>
        <input  name="products[]" type="checkbox" id="14px">
        <label for="14px">14px</label><br>
    </div>
        
    <div>
        <h3>Color</h3>
        <input  name="products[]" type="checkbox" value="white" id="color-white">
        <label for="color-white">White</label> <br>
        <input  name="products[]" type="checkbox" value="black" id="color-black">
        <label for="color-black">Black</label> <br>
        <input  name="products[]" type="checkbox" value="blue" id="color-blue">
        <label for="color-blue">Blue</label> <br>
        <input  name="products[]" type="checkbox" value="darkblue" id="color-darkblue">
        <label for="color-darkblue">Darkblue</label> <br>
        <input  name="products[]" type="checkbox" value="mint" id="color-mint">
        <label for="color-mint">Mint</label> <br>
    </div>
                
    <div>
        <h3>Price</h3>
        <input  name="products[]" type="range" id="price-range" min="1" max="1000" style="width: 175px;">
    </div>
    </div>
    <br> <br> <br>
    <div class= "checkboxes">
     <div>
        <h3>Brand</h3>
        <input  name="products[]" type="checkbox" id="samsung">
        <label for="samsung">Samsung</label> <br>
        <input  name="products[]"type="checkbox" id="sony">
        <label for="sony">Sony</label> <br>
        <input  name="products[]" type="checkbox" id="iphone">
        <label for="iphone">Iphone</label> <br>
        <input  name="products[]" type="checkbox" id="macbook">
        <label for="macbook">Macbook</label> <br>
        <input  name="products[]" type="checkbox" id="hp">
        <label for="hp">HP</label> <br>
        <input  name="products[]" type="checkbox" id="huawei">
        <label for="huawei">Huawei</label> <br>
        <input  name="products[]" type="checkbox" id="honor">
        <label for="honor">Honor</label> <br>
        <input  name="products[]" type="checkbox" id="motorola">
        <label for="motorola">Motorola</label> <br>
        <input  name="products[]" type="checkbox" id="lenovo">
        <label for="lenovo">Lenovo</label> <br>
    </div>

    <div>
        <h3>Manufacturer</h3>
        <input  name="products[]" type="checkbox" id="samsung">
        <label for="samsung">Samsung</label> <br>
        <input  name="products[]" type="checkbox" id="sony">
        <label for="sony">Sony</label> <br>
        <input  name="products[]" type="checkbox" id="apple">
        <label for="apple">Apple</label> <br>
        <input  name="products[]" type="checkbox" id="hp">
        <label for="hp">HP</label> <br>
        <input  name="products[]" type="checkbox" id="huawei">
        <label for="huawei">Huawei</label> <br>
        <input  name="products[]" type="checkbox" id="lenovo">
        <label for="lenovo">Lenovo</label> <br>
    </div>
    </div>
    <br><br>
        <input type="button" name="button" value="Submit"> 
        <br><br>

    </form>

</body>
</html>

<script>

    var productsList = <?php echo json_encode($products) ?>;


    function myFunction(myCheckbox) {
    var products;
    var productsList = document.getElementsByColor("products");
    for (products = 0; products < list.length; ++products) {
        productsList[products].style.display ="none";
        if (productsList[products].classList.contains(myCheckbox.value)) {
            productsList[products].style.display ="block";
      }
     }
    }
</script>

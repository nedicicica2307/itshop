<?php
require_once 'db.php';
$result = "";
$orders = isset($orders)? $orders:[];
class process {
	 private $db;

    
	 private $INSERTORDER = "INSERT INTO orders (name, email, quantity) VALUES (?,?,?)";
	 
	public function insertOrder ($name, $email, $quantity) {
	
        $statement = $this->db->prepare($this->INSERTORDER);
		$statement->bindValue(1, $name);
		$statement->bindValue(2, $email);
		$statement->bindValue(3, $quantity);
		$statement->execute();
	}
    }
?>
<?php

require_once 'loginDAO.php';

$action = isset($_REQUEST["action"])? $_REQUEST["action"] : ""; 


if ($_SERVER['REQUEST_METHOD']=="POST"){
   
    

    header("Location: login.php"); 
    die();
    
} elseif ($_SERVER['REQUEST_METHOD']=="GET"){
    if ($action == 'addToCart') {
        
        $product = isset($_GET["product"])? unserialize($_GET["product"]) : "";
       
        if(!isset($_SESSION)) session_start(); 

        if(!isset($_SESSION['cart'])) $_SESSION['cart']=[]; 

        $_SESSION['cart'][] = $product;

       
       $msg = 'Successfuly added';
       include_once 'products.php';
        die();
    } elseif ($action == 'removeFromCart') {
        
        $product = isset($_GET["product"])? unserialize($_GET["product"]) : "";
      
        if(!isset($_SESSION)) session_start(); 
        if(!isset($_SESSION['cart'])) $_SESSION['cart']=[]; 

        if (($key = array_search($product, $_SESSION['cart'])) !== false) {
            unset($_SESSION['cart'][$key]);
        }

        $msg = 'Successfuly deleted';
        include_once 'cart.php';
    } elseif ($action == 'emptyCart') {
        if(!isset($_SESSION)) session_start(); 
        unset($_SESSION['cart']);
    
        $msg = 'Cart deleted';
        header('Location: cart.php');
    } elseif ($action == 'confirmShoping') {
       
    } else{
    
        header("Location: login.php"); 
        die();
    }
} else {
   
    header("Location: login.php");
    die();
}


function test_input($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>
<?php
$msg = isset($msg) ? $msg: "";
if(!isset($_SESSION)) session_start(); 
var_dump($_SESSION);

$products = isset($_SESSION['cart'])?$_SESSION['cart']:[];
?>
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>SHOP</title>
        </head>

        <body>
            <h2>PRODUCTS</h2>
            <?php if(count($products)>0){ ?>
            <table border="1px;" align="center" width="60%">
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Type</th>
                    <th>Action</th>
                </tr>
                <?php foreach($products as $product){?>
                <tr>
                    <td><?= $product['name'] ?></td>
                    <td><?= $product['price'] ?></td>
                    <td><?= $product['product_type'] ?></td>
                    <td><a href='CartController.php?action=removeFromCart&product=<?= serialize ($product)?>'>REMOVE FROM CART</a></td>
                </tr>
                <?php }?>
            </table>
            <?php } else { ?>
                <h2>CART EMPTY</h2>
            <?php }  ?>
            <br>
            <?= $msg ?>
            <br>
            <a href='CartController.php?action=emptyCart'>EMPTY CART</a><br>         
                    
            <a href="login.php">CONTINUE SHOPING</a><br>

            <a href='CartController.php?action=confirmShoping'>CONFIRM SHOPING</a><br> 
            <a href="LoginController.php?action=logout">LOGOUT</a><br>
        </body>

        </html>
<?php
    

?>
<?php $_SESSION['last-active'] = time(); ?>
<br>

<?php

require'loginDAO.php';

$action = isset($_REQUEST["action"])? $_REQUEST["action"] : ""; 


if ($_SERVER['REQUEST_METHOD']=="POST"){
    
    if ($action == 'LOGIN') {
        $username = isset($_POST["username"])? test_input($_POST["username"]) : ""; 
        $password = isset($_POST["password"])? test_input($_POST["password"]) : ""; 
        
        $logindao = new loginDAO();
        $user = $logindao->selectUserByUsernameAndPassword($username,$password);
        if($user){
          

          
            session_start();
            $_SESSION['user'] = $user;
            $_SESSION['last-active'] = time();
          
            if (isset($_POST['rememberMe'])) {
                $userCookie = array('username' => $_POST['username'], 'password' => $_POST['password']);
            
                setcookie("userJSON", json_encode($userCookie), time() + 20, "/");
            }

            include_once $user['type'].'.php';
        }else{
            $msg = "Wrong parametars";
            include_once 'login.php';
        }
        
    } elseif ($action == 'REGISTER') {
     
        $ime = isset($_POST["name"])? test_input($_POST["name"]) : ""; 
        $prezime = isset($_POST["lastname"])? test_input($_POST["lastname"]) : ""; 
        $email = isset($_POST["email"])? test_input($_POST["email"]) : ""; 
        $username = isset($_POST["username"])? test_input($_POST["username"]) : ""; 
        $password = isset($_POST["password"])? test_input($_POST["password"]) : ""; 
        $repeatPassword = isset($_POST["repeatPassword"])? test_input($_POST["repeatPassword"]) : ""; 
        $robot = isset($_POST["robot"])? test_input($_POST["robot"]) : ""; 

        if($robot == "") header("location: register.php"); 

        if(!($name == "" || $lastname == "" || $email == "" || $username == "" || $password == "" || $repeatPassword == "" )){
            if($password == $repeatPassword){ 
                $logindao = new loginDAO();
                if(!$logindao->selectUserByUsername($username)){ 
                 
                    if($logindao->insertUserWithAtributes($name, $lastname, $email,$username, $password)==false){
                        $msg = "Registration error.Try later!";
                    }else{
                        $msg = "Successful registration, you may continue";
                    }
                    include_once 'register.php';

                }else{
                    $msg = "Username '$username' is already in use";
                    include_once 'register.php';
                }
            }else{
                $msg = "Paswords must fit";
                include_once 'register.php';
            }
        }else{
            $msg = "You have to fill all inputs";
            include_once 'register.php';
        }




    } 
    
} elseif ($_SERVER['REQUEST_METHOD']=="GET"){
    
    if ($action == 'logout') {
        session_start();
        session_unset();
        session_destroy();
    
        header('Location: login.php');
    } elseif ($action == 'akcijaGet2'){
      
    }elseif ($action == 'akcijaGet3'){
    
    }
    
} else {
    
    header("Location: login.php"); 
    die();
}


function test_input($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>
<?php
require_once 'db.php';

class DAO {
	private $db;

	// za 2. nacin resenja
	private $INSERT_ELECTRICS = "INSERT INTO filter (id, type, price, size) VALUES (?,?, ?, ?)";
    public function __construct()
	{
		$this->db = DB::createInstance();
	}
    public function insertElectrics($id, $name, $type, $price, $size)
	{
		
		$statement = $this->db->prepare($this->INSERT_ELECTRICS);
		$statement->bindValue(1, $id);
		$statement->bindValue(2, $name);
		$statement->bindValue(2, $type);
		$statement->bindValue(3, $price);
		$statement->bindValue(4, $size);
		
		$statement->execute();
	}
}
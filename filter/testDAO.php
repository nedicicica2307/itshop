<?php
    //phpinfo();
    //die();
    require_once 'DAO.php';

    $dao = new DAO();
    $dao->insertElectrics("Samsung","phone",300,6);
   
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TEST DAO</title>
</head>
<body>
    <h2>TEST DAO</h2>

    <table border=1 width="80%" align="center">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Type</th>
            <th>Price</th>
            <th>Size</th>
        </tr>
        <?php foreach ($electrics as $pom ){?>
        <tr>
            <td><?= $pom['id'] ?></td>
            <td><?= $pom['name'] ?></td>
            <td><?= $pom['type'] ?></td>
            <td><?= $pom['price'] ?></td>
            <td><?= $pom['size'] ?></td>
        </tr>
        <?php }?>
    </table>
</body>
</html>
